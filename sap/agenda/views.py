from django.shortcuts import render
from rest_framework import generics, status
from .serializers import *

from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Domicilio

# Create your views here.

# CON VISTAS GENERICAS

# Muestra todos los domicilios


# Ejemplo http://localhost:8000/api/dom
class DomicilioAllView(generics.ListAPIView):
    serializer_class = DomicilioSerializer
    # serializer_class = DomicilioSerializer2
    queryset = Domicilio.objects.all()

# Se pasa un id por el browser
# Ejemplo http://localhost:8000/api/dom/1
class DomicilioView(generics.ListAPIView):
    serializer_class = DomicilioSerializer
    # serializer_class = DomicilioSerializer2
    def get_queryset(self):
        id = self.kwargs['id']
        return Domicilio.objects.filter(id=id)

class DomicilioDetail(generics.RetrieveAPIView):
    serializer_class = DomicilioSerializer2
    queryset = Domicilio.objects.filter() #Para que funcione, se debe pasar un parámetro llamado pk

#Creando un nuevo domicilio
class DomicilioCreate(generics.CreateAPIView):
    serializer_class =  DomicilioSerializerCRUD 
class DomicilioCreate2(generics.CreateAPIView):
    serializer_class =  DomicilioSerializerCRUD2

#Eliminando un domicilio
class DomicilioDelete(generics.DestroyAPIView):
    serializer_class = DomicilioSerializer2
    queryset = Domicilio.objects.all() #Para que funcione, nuevamente se debe pasar un parámetro llamado pk

# Modificando un domicilio
class DomicilioUpdate(generics.UpdateAPIView):
    serializer_class = DomicilioSerializer #Para que funcione, se debe pasar un parámetro llamado pk
    queryset = Domicilio.objects.all()

class DomicilioCrudGenerico(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DomicilioSerializer #Para que funcione, se debe pasar un parámetro llamado pk
    queryset = Domicilio.objects.all()

# SIN VISTAS GENERICAS

# Muestra todos los domicilios (GET) e inserta nuevos (POST)
# Ejemplo http://localhost:8000/api/dom
@api_view(['GET', 'POST'])
def list_domicilios(request):
    if request.method == 'GET':
        domicilio = Domicilio.objects.all()
        serializer = DomicilioSerializer(domicilio, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DomicilioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Se pasa un id por el browser para consultar un registro (GET) o eliminar (DELETE)
# Ejemplo http://localhost:8000/api/dom2/1
@api_view(['GET', 'DELETE', 'PUT'])
def list_domicilio(request, id):
    if request.method == 'GET':
        domicilio = Domicilio.objects.get(id=id)
        serializer = DomicilioSerializer(domicilio)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        domicilio = Domicilio.objects.get(id=id)  
        try:
            domicilio.delete()
            return Response({'mensaje': 'Registro '+str(id)+' eliminado exitosamente'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'mensaje': 'Registro '+str(id)+' no pudo ser eliminado, posiblemente no existe o hubo error interno'}, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        domicilio = Domicilio.objects.get(id=id)
        serializer = DomicilioSerializer(domicilio, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)



# Vistas para Persona

# Con vistas genericas
class PersonaAllView(generics.ListAPIView):
    serializer_class = PersonaSerializer
    queryset = Persona.objects.all()

class PersonaCreate(generics.CreateAPIView):
    serializer_class =  PersonaSerializer 

class PersonaUpdate(generics.UpdateAPIView):
    serializer_class = PersonaSerializer #Para que funcione, se debe pasar un parámetro llamado pk
    queryset = Persona.objects.all()

class PersonaDelete(generics.DestroyAPIView):
    serializer_class = PersonaSerializer
    queryset = Persona.objects.all()

class PersonaCrudGenerico(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PersonaSerializer #Para que funcione, se debe pasar un parámetro llamado pk
    queryset = Persona.objects.all()

#Sin vistas genéricas
@api_view(['GET', 'POST'])
def list_personas(request):
    if request.method == 'GET':
        persona = Persona.objects.all()
        serializer = PersonaSerializer(persona, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = PersonaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		
@api_view(['GET', 'DELETE', 'PUT'])
def list_persona(request, id):
    if request.method == 'GET':
        persona = Persona.objects.get(id=id)
        serializer = PersonaSerializer(persona)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        persona = Persona.objects.get(id=id)  
        try:
            persona.delete()
            return Response({'mensaje': 'Registro '+str(id)+' eliminado exitosamente'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'mensaje': 'Registro '+str(id)+' no pudo ser eliminado, posiblemente no existe o hubo error interno'}, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        persona = Persona.objects.get(id=id)
        serializer = PersonaSerializer(persona, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)	

#Sin vista genérica, mostrando ahora los id
@api_view(['GET', 'POST'])
def list_personas2(request):
    if request.method == 'GET':
        persona = Persona.objects.all()
        serializer = PersonaSerializer3(persona, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = PersonaSerializer3(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		
@api_view(['GET', 'DELETE', 'PUT'])
def list_persona2(request, id):
    if request.method == 'GET':
        persona = Persona.objects.get(id=id)
        serializer = PersonaSerializer3(persona)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        persona = Persona.objects.get(id=id)  
        try:
            persona.delete()
            return Response({'mensaje': 'Registro '+str(id)+' eliminado exitosamente'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'mensaje': 'Registro '+str(id)+' no pudo ser eliminado, posiblemente no existe o hubo error interno'}, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        persona = Persona.objects.get(id=id)
        serializer = PersonaSerializer3(persona, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)	