from django.db import models

# Create your models here.
class Domicilio(models.Model):
    address = models.CharField("direccion", max_length=100)
    number =models.IntegerField("numero")
    city = models.CharField("ciudad", max_length=100)

    def __str__(self):
        return self.address, self.number, self.city
    

    class Meta:
        db_table = "domicilios"


class Persona(models.Model):
    name = models.CharField("nombre", max_length=50)
    fono = models.CharField("fono", max_length=12)
    domicilio_id=models.ForeignKey(Domicilio, on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = "personas"
