from django.urls import path
from django.http import HttpResponse
from .views import *

def aaa(self):
    return HttpResponse('HolaMundo')


urlpatterns = [
    # *** DOMICILIOS ***

    # enlistar domicilios
    path('hola/', aaa),
    path('dom', DomicilioAllView.as_view()), 
    path('dom/<int:id>', DomicilioView.as_view()),
    
    path('dom3/<pk>', DomicilioDetail.as_view()), #Para que el retrieveAPIView funcione, debe pasarse un <pk> a diferencia de los anteriores que fueron personalizados

    # crear nuevo domicilio
    path('newdom', DomicilioCreate.as_view()),
    path('newdom2', DomicilioCreate2.as_view()),

    #Eliminando un registro
    path('deletedom/<pk>', DomicilioDelete.as_view()),

    # actualizando registro
    path('updatedom/<pk>', DomicilioUpdate.as_view()),

    #CRUD con vista genérica
    path('domicilio/<pk>', DomicilioCrudGenerico.as_view()),

    #CRUD sin vistas genéricas
    
    path('dom2', list_domicilios), #Maestro que muestra todos los registros (GET) o inserta uno nuevo (POST)
    path('dom2/<int:id>', list_domicilio), #Muestra un domicilio (GET), o elimina uno (DELETE) o lo actualiza (UPDATE)

    # ** PERSONAS **
    #Vistas genéricas
    path('persona', PersonaAllView.as_view()),
    path('persona_new', PersonaCreate.as_view()),
    path('persona/<pk>', PersonaUpdate.as_view()),
    path('persona_delete/<pk>', PersonaDelete.as_view()),
    path('persona_crud/<pk>', PersonaCrudGenerico.as_view()),
    #Sin vistas genéricas
    path('persona2', list_personas), #Maestro que muestra todos los registros (GET) o inserta uno nuevo (POST)
	path('persona2/<int:id>', list_persona), #Muestra un domicilio (GET), o elimina uno (DELETE) o lo actualiza (UPDATE)
    #Ahora sin vista genérica pero mostrando el id de los serializer
    path('persona3', list_personas2), #Maestro que muestra todos los registros (GET) o inserta uno nuevo (POST)
	path('persona3/<int:id>', list_persona2), #Muestra un domicilio (GET), o elimina uno (DELETE) o lo actualiza (UPDATE)
]
