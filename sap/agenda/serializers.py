from rest_framework import serializers
from .models import *

# Serializadores de Domicilio

'''
Entrada de los 2 sigtes serializadores
{
    'id':
    'address':,
    'number':,
    'city':
}

'''
class DomicilioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domicilio
        fields = '__all__'  # Todos los campos        

class DomicilioSerializer2(serializers.Serializer):
    id = serializers.IntegerField()
    address = serializers.CharField(max_length=100)
    number = serializers.IntegerField()
    city = serializers.CharField(max_length=100)

'''
Entrada de os 2 sigtes serializador
{
    'address':,
    'number':,
    'city':
}
Observación: al usar serializers.Serializer, se debe especificar el método created. Cuando son serializers.ModelSerializer, no hace falta

'''

class DomicilioSerializerCRUD(serializers.ModelSerializer):
    class Meta:
        model = Domicilio
        fields = ('id','address', 'number', 'city') #lo que no se especifique no saldrá

class DomicilioSerializerCRUD2(serializers.Serializer):
    address = serializers.CharField(max_length=100)
    number = serializers.IntegerField()
    city = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return Domicilio.objects.create(**validated_data)


# Serializadores de Persona

class DomicilioSinIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domicilio
        fields = ('address', 'number', 'city') # No mostrará el id

'''
Ejemplo del siguiente Serializer
[
    {
        "nombre": "juan",
        "fono": "1223456",
        "domicilio": {
            "address": "tommu",
            "number": 11111,
            "city": "temuco"
        }
    }
]
'''

class PersonaSerializer(serializers.ModelSerializer):
    domicilio = DomicilioSinIdSerializer(source ='domicilio_id') # Debe estar en el domicilio. Acá se renombra el domicilio_id con domicilio
    # domicilio_id = DomicilioSinIdSerializer() #Con el de arriba se evita usar domicilio_id y se usa domicilio
    nombre = serializers.CharField(source='name') #Para renombrar localmente

    # Ejemplo de multioperar en un campo, varios
    '''
    nombre_fono = serializers.SerializerMethodField()

    def nombre_fono(self, obj):
        return str(obj.name)+' '+str(obj.fono)

    Ya en los fields, se puede agregar!!!
    '''
    #Ejemplo de agregar atributos fuera del modelo con valor default
    '''
    activo = serializers.BooleanField(required=False, default=False) #El default es opcional
    '''

    class Meta:
        model = Persona
        fields = ('nombre', 'fono', 'domicilio')

    def create(self, validated_data):
        domicilio_data = validated_data.pop('domicilio_id', None)
        if domicilio_data:
            domicilio = Domicilio.objects.get_or_create(**domicilio_data)[0]
            validated_data['domicilio_id'] = domicilio
        #print(validated_data)
        return Persona.objects.create(**validated_data)

    def update(self, instance, validated_data):
        domicilio_data = validated_data.pop('domicilio_id', None)
        if domicilio_data:
            domicilio = Domicilio.objects.get_or_create(**domicilio_data)[0] #Si no existe lo creará
            validated_data['domicilio_id'] = domicilio
        # Inicio de la actualización
        instance.name = validated_data.get('name', instance.name)
        instance.fono = validated_data.get('fono', instance.fono)
        instance.domicilio_id = validated_data.get('domicilio_id', instance.domicilio_id)
        instance.save()
        return instance

'''
Retorno del serializer siguiente
[
    {
        "id": 1,
        "name": "juan",
        "fono": "1223456",
        "domicilio_id": 2
    }
]
'''
class PersonaSerializer2(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = '__all__'

'''
El siguiente serializer retornará:

'''
class PersonaSerializer3(serializers.ModelSerializer):
    domicilio = DomicilioSerializer(source ='domicilio_id') # Debe estar en el domicilio. Acá se renombra el domicilio_id con domicilio
    nombre = serializers.CharField(source='name') #Para renombrar localmente

    class Meta:
        model = Persona
        fields = ('id', 'nombre', 'fono', 'domicilio')

    def create(self, validated_data):
        domicilio_data = validated_data.pop('domicilio_id', None)
        if domicilio_data:
            domicilio = Domicilio.objects.get_or_create(**domicilio_data)[0]
            validated_data['domicilio_id'] = domicilio
        #print(validated_data)
        return Persona.objects.create(**validated_data)

    def update(self, instance, validated_data):
        domicilio_data = validated_data.pop('domicilio_id', None)
        if domicilio_data:
            domicilio = Domicilio.objects.get_or_create(**domicilio_data)[0] #Si no existe lo creará
            validated_data['domicilio_id'] = domicilio
        # Inicio de la actualización
        instance.name = validated_data.get('name', instance.name)
        instance.fono = validated_data.get('fono', instance.fono)
        instance.domicilio_id = validated_data.get('domicilio_id', instance.domicilio_id)
        instance.save()
        return instance